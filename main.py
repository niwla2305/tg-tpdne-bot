#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import time
import os

from telegram.ext import Updater
from ThisPersonDoesNotExistAPI.thispersondoesnotexist import get_online_person, save_picture

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi! I am working for @unrealpersons')


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    updater = Updater(os.environ.get("TOKEN"), use_context=True)

    dp = updater.dispatcher
    dp.add_error_handler(error)

    updater.start_polling()
    print("Bot started")
    while True:
        try:
            save_picture(get_online_person(), "tmp.jpeg")
            updater.bot.send_photo("@unrealpersons", open("tmp.jpeg", "rb"),
                                caption="@unrealpersons Photo created using thispersondoesnotexist.com")
            os.remove("tmp.jpeg")
        except:
            print("Something went wrong")
        time.sleep(60*60)


if __name__ == '__main__':
    main()
